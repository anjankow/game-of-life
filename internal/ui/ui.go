package ui

import (
	"fmt"
	"game-of-life/internal/logic"
)

func PrintBoard(g logic.GameBoard) {
	for i := 0; i < g.GetHeight(); i++ { // rows

		printHorizontalLine(g.GetWidth())

		fmt.Print("|")
		for j := 0; j < g.GetWidth(); j++ {
			if g.IsLive(i, j) {
				fmt.Print("#")
			} else {
				fmt.Print(" ")
			}
			fmt.Print("|")
		}

		fmt.Println("")
	}
	printHorizontalLine(g.GetWidth())

}

func printHorizontalLine(width int) {
	fmt.Print("+")
	for j := 0; j < width; j++ {
		fmt.Print("-+")
	}
	fmt.Println("")
}
