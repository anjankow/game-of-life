package server

import (
	"fmt"
	"game-of-life/internal/controller"
	"game-of-life/internal/ui"
	"net/http"
	"strings"

	"github.com/gorilla/websocket"
	"go.uber.org/zap"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type worker struct {
	conn *websocket.Conn
	ctrl *controller.Controller
}

func (w worker) processMessages() error {
	for {
		messageType, p, err := w.conn.ReadMessage()
		if err != nil {
			w.ctrl.Logger.Error("read message error", zap.Error(err))
			return err
		}

		var message string
		switch messageType {
		case websocket.BinaryMessage:
			message = string(p)
		case websocket.TextMessage:
			message = string(p)
		default:
			w.ctrl.Logger.Info("no handler for the received message type", zap.Int("message_type", messageType))
			continue
		}

		w.ctrl.Logger.Debug("received ws message", zap.String("message", message), zap.Int("message_type", messageType))

		response, err := w.interpretMessage(message)
		if err != nil {
			w.ctrl.Logger.Error("processing the message failed", zap.Error(err))
			continue
		}

		rspMsg := w.makeResponseMessage(response)
		if err = w.conn.WriteMessage(websocket.TextMessage, []byte(rspMsg)); err != nil {
			w.ctrl.Logger.Error("writing the response failed", zap.Error(err))
			continue
		}

		w.ctrl.Logger.Debug("message handled successfully", zap.String("response", rspMsg))

		if response != controller.ResponseNoAction {
			board := w.ctrl.GetGameBoard()
			fmt.Println("")
			ui.PrintBoard(board)
			fmt.Println("")
		}
	}
}

func (w worker) boardRequest() (controller.Response, error) {
	return controller.ResponseUpdate, nil
}

func (w worker) interpretMessage(message string) (controller.Response, error) {
	if message == "board?" {
		return w.boardRequest()
	}

	if message == "next" {
		return w.ctrl.ProcessNext()
	}

	if message == "start" {
		return w.ctrl.ProcessStart()
	}

	if message == "new" {
		return w.ctrl.ProcessNew()
	}

	words := strings.Split(message, " ")
	if len(words) == 3 && words[0] == "toggle" {
		return w.ctrl.ProcessSetFields(words[1], words[2])
	}

	if len(words) == 3 && words[0] == "resize" {
		return w.ctrl.ProcessResize(words[1], words[2])
	}

	return controller.ResponseNoAction, nil
}

func (w worker) makeResponseMessage(r controller.Response) string {

	if r == controller.ResponseUpdate {
		board := w.ctrl.GetGameBoard()
		response := "update " + fmt.Sprint(w.ctrl.GetGameBoard().GetHeight()) + " " + fmt.Sprint(w.ctrl.GetGameBoard().GetWidth()) + " "

		boolToIntString := func(b bool) string {
			boolInt := 0
			if b {
				boolInt = 1
			}
			return fmt.Sprint(boolInt)
		}

		for i := 0; i < board.GetHeight(); i++ {
			response += "|"
			for j := 0; j < board.GetWidth(); j++ {
				response += "," + boolToIntString(board.IsLive(i, j))
			}
		}
		return response
	}

	return "no_action"
}

func wsHandler(c *controller.Controller, w http.ResponseWriter, r *http.Request) (int, error) {

	upgrader.CheckOrigin = func(r *http.Request) bool {
		return true
	}

	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		c.Logger.Warn("upgrade failed", zap.Error(err))
		return http.StatusBadRequest, err
	}

	workerInstance := worker{
		conn: ws,
		ctrl: c,
	}
	if err = workerInstance.processMessages(); err != nil {
		return http.StatusInternalServerError, err
	}

	return http.StatusOK, nil
}

func handler(c *controller.Controller, w http.ResponseWriter, r *http.Request) (int, error) {
	w.Write([]byte("handled!"))
	return http.StatusOK, nil
}
