package server

import (
	"game-of-life/internal/config"
	"game-of-life/internal/controller"
	"log"
	"net"
	"net/http"

	"go.uber.org/zap"
)

type server struct {
	logger     *zap.Logger
	ctrl       *controller.Controller
	httpServer *http.Server
}

type appHandler struct {
	ctrl   *controller.Controller
	Handle AppHandleFunc
}

func NewServer(logger *zap.Logger, c *controller.Controller) server {
	return server{
		logger: logger,
		ctrl:   c,
	}
}

type AppHandleFunc func(*controller.Controller, http.ResponseWriter, *http.Request) (int, error)

func (ser server) registerHandlers() {
	http.Handle("/", appHandler{ctrl: ser.ctrl, Handle: handler})
	http.Handle("/ws", appHandler{ctrl: ser.ctrl, Handle: wsHandler})
}

func (ser server) Run() error {
	hostPort := net.JoinHostPort("localhost", config.DefaultPort)
	ser.httpServer = &http.Server{
		Addr:     hostPort,
		ErrorLog: zap.NewStdLog(ser.logger),
	}

	ser.registerHandlers()

	return ser.httpServer.ListenAndServe()
}

func (appHndl appHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	appHndl.ctrl.Logger.Debug("Request received", zap.String("method", r.Method), zap.String("url", r.URL.Path), zap.String("content-type", r.Header.Get("Content-Type")))
	status, err := appHndl.Handle(appHndl.ctrl, w, r)
	appHndl.ctrl.Logger.Debug("Request served")

	if err != nil {
		log.Printf("HTTP %d: %q", status, err)
		switch status {
		case http.StatusNotFound:
			http.NotFound(w, r)
		case http.StatusInternalServerError:
			http.Error(w, http.StatusText(status), status)
		default:
			http.Error(w, http.StatusText(status), status)
		}
		return
	}
}
