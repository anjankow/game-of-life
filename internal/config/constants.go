package config

const (
	EnvFile     = ".env"
	JsonCfgPath = "config/config.json"

	DefaultNumOfThreads = 5
	DefaultPort         = "7777"

	MinHeight = 2
	MaxHeight = 1000

	MinWidth = 2
	MaxWidth = 1000
)
