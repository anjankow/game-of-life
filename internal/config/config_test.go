package config_test

import (
	"game-of-life/internal/config"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	envPath = "test_configs/.env"
)

func TestParseConfigNoFile(t *testing.T) {
	jsonPath := "test_configs/invalid_file.json"
	_, err := config.ParseConfig(jsonPath, envPath)
	assert.NotNil(t, err)
}

func TestParseConfigInvalidFile(t *testing.T) {
	jsonPath := "test_configs/test_config_invalid.json"
	_, err := config.ParseConfig(jsonPath, envPath)
	assert.NotNil(t, err)
	t.Log(err)
}

func TestParseConfigValidFile(t *testing.T) {
	jsonPath := "test_configs/test_config_valid.json"
	cfg, err := config.ParseConfig(jsonPath, envPath)
	assert.Nil(t, err)
	assert.Equal(t, 5, cfg.NumOfThreads)
	assert.Equal(t, 12, cfg.Heigth)
	assert.Equal(t, 11, cfg.Width)
	assert.Equal(t, ">=", cfg.LiveToDead[1].Operator)
}
