package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/spf13/viper"
	"go.uber.org/multierr"
)

type Condition struct {
	Operator       string `json:"live_neighbs_cmp"`
	LiveNeighbsNum int    `json:"live_neighbs_num"`
}

type Config struct {
	// internal config
	NumOfThreads int

	// config to be modified by the user
	Heigth     int         `json:"height"`
	Width      int         `json:"width"`
	DeadToLive []Condition `json:"dead_to_live"`
	LiveToDead []Condition `json:"live_to_dead"`
}

func getJsonConfig(path string) (Config, error) {
	jsonFile, err := os.Open(path)
	if err != nil {
		return Config{}, err
	}
	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return Config{}, err
	}

	var cfg Config
	if err = json.Unmarshal(byteValue, &cfg); err != nil {
		return Config{}, err
	}

	return cfg, cfg.Validate()
}

func addEnvConfig(cfg Config, path string) (Config, error) {
	viper.SetConfigFile(path)
	if err := viper.ReadInConfig(); err != nil {
		return cfg, err
	}

	cfg.NumOfThreads = viper.GetInt("NUMBER_OF_THREADS")
	if cfg.NumOfThreads == 0 {
		cfg.NumOfThreads = DefaultNumOfThreads
	}

	return cfg, nil
}

func ParseConfig(jsonPath, envPath string) (Config, error) {
	cfg, err := getJsonConfig(jsonPath)
	if err != nil {
		return Config{}, err
	}

	return addEnvConfig(cfg, envPath)
}

func (cfg Config) Validate() error {
	var err error

	if cfg.Heigth < 3 {
		err = multierr.Append(err, fmt.Errorf("height should be equal at least 3"))
	}

	if cfg.Width < 3 {
		err = multierr.Append(err, fmt.Errorf("width should be equal at least 3"))
	}

	if len(cfg.DeadToLive) < 1 {
		err = multierr.Append(err, fmt.Errorf("at least one dead_to_live rule should be present"))
	}

	if len(cfg.LiveToDead) < 1 {
		err = multierr.Append(err, fmt.Errorf("at least one live_to_dead rule should be present"))
	}

	return err
}
