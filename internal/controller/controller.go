package controller

import (
	"errors"
	"game-of-life/internal/config"
	"game-of-life/internal/logic"
	"strconv"

	"go.uber.org/zap"
)

type Response int

const (
	ResponseNoAction Response = 0
	ResponseUpdate   Response = 1
)

type Controller struct {
	Logger *zap.Logger
	cfg    config.Config
	game   *logic.Game
}

func NewController(logger *zap.Logger, cfg config.Config, game *logic.Game) (Controller, error) {
	if logger == nil {
		return Controller{}, errors.New("logger instance invalid")
	}

	if err := cfg.Validate(); err != nil {
		return Controller{}, err
	}

	if game == nil {
		return Controller{}, errors.New("game instance invalid")
	}

	return Controller{
		Logger: logger,
		cfg:    cfg,
		game:   game,
	}, nil
}

func (c *Controller) ProcessStart() (Response, error) {
	if err := c.game.Start(); err != nil {
		return ResponseNoAction, err
	}

	return ResponseUpdate, nil
}

func (c *Controller) ProcessNext() (Response, error) {
	if err := c.game.NextMove(); err != nil {
		return ResponseNoAction, err
	}

	return ResponseUpdate, nil
}

func (c *Controller) ProcessNew() (Response, error) {
	c.game.New()

	return ResponseUpdate, nil
}

func (c *Controller) ProcessSetFields(xCoordinate, yCoordinate string) (Response, error) {
	x, xErr := strconv.Atoi(xCoordinate)
	y, yErr := strconv.Atoi(yCoordinate)
	if xErr != nil || yErr != nil {
		return ResponseNoAction, errors.New("casting the coordinates to int failed")
	}

	if err := c.game.ToggleCellByUser(x, y); err != nil {
		return ResponseNoAction, err
	}

	return ResponseUpdate, nil
}

func (c *Controller) ProcessResize(xCoordinate, yCoordinate string) (Response, error) {
	x, xErr := strconv.Atoi(xCoordinate)
	y, yErr := strconv.Atoi(yCoordinate)
	if xErr != nil || yErr != nil {
		return ResponseNoAction, errors.New("casting the coordinates to int failed")
	}

	if err := c.game.ChangeSize(x, y); err != nil {
		return ResponseNoAction, err
	}

	return ResponseUpdate, nil
}

func (c *Controller) GetGameBoard() logic.GameBoard {
	return c.game.GetGameBoard()
}
