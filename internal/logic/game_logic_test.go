package logic_test

import (
	"fmt"
	"game-of-life/internal/config"
	"game-of-life/internal/logic"
	"game-of-life/internal/ui"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

var (
	board = [][]bool{
		{true, false, true, false, false, false, false, false, true, false, false, false},
		{true, false, true, false, false, false, false, false, false, false, false, false},
		{true, true, true, false, false, false, false, true, true, false, false, false},
		{true, true, true, false, false, false, false, true, true, false, false, false},
		{true, false, true, false, false, false, false, false, true, false, false, false},
		{true, false, true, false, false, false, false, true, true, false, false, false},
	}
)

func setInitialBoard(t *testing.T, board *[][]bool, g logic.Game) {
	t.Helper()
	for i := 0; i < g.GetGameBoard().GetHeight(); i++ {
		for j := 0; j < g.GetGameBoard().GetWidth(); j++ {
			if (*board)[i][j] {
				g.ToggleCellByUser(i, j)
			}
		}
	}
}

func TestGameEmptyBoard(t *testing.T) {
	t.Parallel()

	cfg := config.Config{
		NumOfThreads: 7,
		Heigth:       12,
		Width:        9,
		DeadToLive: []config.Condition{
			{
				LiveNeighbsNum: 0,
				Operator:       "=",
			},
		},
		LiveToDead: []config.Condition{
			{
				LiveNeighbsNum: 0,
				Operator:       ">",
			},
		},
	}
	game := logic.NewGame(zap.NewNop(), cfg)

	assert.NoError(t, game.Start())

	board := game.GetGameBoard()
	ui.PrintBoard(game.GetGameBoard())

	assert.False(t, board.IsLive(0, 5))
	assert.False(t, board.IsLive(10, 8))
	assert.False(t, board.IsLive(4, 0))

	game.NextMove()
	board = game.GetGameBoard()
	ui.PrintBoard(game.GetGameBoard())

	assert.True(t, board.IsLive(0, 5))
	assert.True(t, board.IsLive(10, 8))
	assert.True(t, board.IsLive(4, 0))

	game.NextMove()
	board = game.GetGameBoard()
	ui.PrintBoard(game.GetGameBoard())

	assert.False(t, board.IsLive(0, 5))
	assert.False(t, board.IsLive(10, 8))
	assert.False(t, board.IsLive(4, 0))

}

func TestGameWithInitialSetup(t *testing.T) {
	cfg := config.Config{
		NumOfThreads: 1,
		Heigth:       6,
		Width:        12,
		DeadToLive: []config.Condition{
			{
				LiveNeighbsNum: 3,
				Operator:       "=",
			},
		},
		LiveToDead: []config.Condition{
			{
				LiveNeighbsNum: 2,
				Operator:       "<",
			},
			{
				LiveNeighbsNum: 3,
				Operator:       ">",
			},
		},
	}

	g := logic.NewGame(zap.NewNop(), cfg)

	setInitialBoard(t, &board, g)

	assert.NoError(t, g.Start())
	assert.Equal(t, 22, g.GetGameBoard().CountLivingCells(), 22)
	ui.PrintBoard(g.GetGameBoard())
	fmt.Println("")

	g.NextMove()
	assert.Equal(t, 16, g.GetGameBoard().CountLivingCells())
	ui.PrintBoard(g.GetGameBoard())
	fmt.Println("")

	g.NextMove()
	assert.Equal(t, 15, g.GetGameBoard().CountLivingCells())
	ui.PrintBoard(g.GetGameBoard())
	fmt.Println("")

	g.NextMove()
	assert.Equal(t, 14, g.GetGameBoard().CountLivingCells())
	ui.PrintBoard(g.GetGameBoard())
}
