package logic_test

import (
	"fmt"
	"game-of-life/internal/config"
	"game-of-life/internal/logic"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

type testset struct {
	x, y       int
	deadToLive []config.Condition
	liveToDead []config.Condition
}

var testBoard = [][]bool{
	{false, false, true, true, false},
	{false, false, true, true, false},
	{false, false, true, true, true},
	{false, false, true, false, true},
	{true, false, true, true, true},
}

var rulesMetTestset = []testset{
	// dead to live
	{
		x: 0,
		y: 0,
		deadToLive: []config.Condition{
			{
				Operator:       ">",
				LiveNeighbsNum: 3,
			},
			{
				Operator:       "=",
				LiveNeighbsNum: 2,
			},
			{
				Operator:       "<",
				LiveNeighbsNum: 1,
			}},
		liveToDead: []config.Condition{{
			Operator:       "<",
			LiveNeighbsNum: 2,
		}},
	},
	{
		x: 0,
		y: 4,
		deadToLive: []config.Condition{{
			Operator:       ">=",
			LiveNeighbsNum: 2,
		}, {
			Operator:       "=",
			LiveNeighbsNum: 0,
		}},
		liveToDead: []config.Condition{{
			Operator:       ">=",
			LiveNeighbsNum: 1,
		}},
	},
	{
		x: 3,
		y: 0,
		deadToLive: []config.Condition{{
			Operator:       "=",
			LiveNeighbsNum: 1,
		}},
		liveToDead: []config.Condition{{
			Operator:       "<",
			LiveNeighbsNum: 8,
		}},
	},
	{
		x: 3,
		y: 3,
		deadToLive: []config.Condition{{
			Operator:       ">",
			LiveNeighbsNum: 7,
		}},
		liveToDead: []config.Condition{{
			Operator:       "<=",
			LiveNeighbsNum: 7,
		}},
	},
	// live to dead
	{
		x: 4,
		y: 0,
		deadToLive: []config.Condition{{
			Operator:       "=",
			LiveNeighbsNum: 0,
		}},
		liveToDead: []config.Condition{{
			Operator:       "=",
			LiveNeighbsNum: 0,
		}},
	},
	{
		x: 4,
		y: 2,
		deadToLive: []config.Condition{{
			Operator:       "=",
			LiveNeighbsNum: 1,
		}},
		liveToDead: []config.Condition{{
			Operator:       ">=",
			LiveNeighbsNum: 2,
		}},
	},
}

var rulesNotMetTestset = []testset{
	// dead to live
	{
		x: 0,
		y: 0,
		deadToLive: []config.Condition{{
			Operator:       ">",
			LiveNeighbsNum: 0,
		}},
		liveToDead: []config.Condition{{
			Operator:       "<",
			LiveNeighbsNum: 2,
		}},
	},
	{
		x: 0,
		y: 4,
		deadToLive: []config.Condition{{
			Operator:       ">=",
			LiveNeighbsNum: 3,
		}, {
			Operator:       "=",
			LiveNeighbsNum: 1,
		}},
		liveToDead: []config.Condition{{
			Operator:       ">=",
			LiveNeighbsNum: 1,
		}},
	},
	{
		x: 3,
		y: 0,
		deadToLive: []config.Condition{{
			Operator:       "=",
			LiveNeighbsNum: 0,
		}},
		liveToDead: []config.Condition{{
			Operator:       "=",
			LiveNeighbsNum: 1,
		}},
	},
	{
		x: 3,
		y: 3,
		deadToLive: []config.Condition{{
			Operator:       "<=",
			LiveNeighbsNum: 7,
		}},
		liveToDead: []config.Condition{{
			Operator:       "<=",
			LiveNeighbsNum: 8,
		}},
	},
	// live to dead
	{
		x: 4,
		y: 0,
		deadToLive: []config.Condition{{
			Operator:       "=",
			LiveNeighbsNum: 1,
		}},
		liveToDead: []config.Condition{{
			Operator:       "=",
			LiveNeighbsNum: 1,
		}},
	},
	{
		x: 4,
		y: 2,
		deadToLive: []config.Condition{{
			Operator:       "=",
			LiveNeighbsNum: 3,
		}},
		liveToDead: []config.Condition{{
			Operator:       ">",
			LiveNeighbsNum: 2,
		}},
	},
}

var ousideOfRangeTestset = []testset{
	{
		x: -1,
		y: 2,
		deadToLive: []config.Condition{{
			Operator:       "=",
			LiveNeighbsNum: 1,
		}},
		liveToDead: []config.Condition{{
			Operator:       "=",
			LiveNeighbsNum: 1,
		}},
	},
	{
		x: 1,
		y: -1,
		deadToLive: []config.Condition{{
			Operator:       "=",
			LiveNeighbsNum: 1,
		}},
		liveToDead: []config.Condition{{
			Operator:       "=",
			LiveNeighbsNum: 1,
		}},
	},
	{
		x: 5,
		y: 0,
		deadToLive: []config.Condition{{
			Operator:       "=",
			LiveNeighbsNum: 1,
		}},
		liveToDead: []config.Condition{{
			Operator:       "=",
			LiveNeighbsNum: 1,
		}},
	},
	{
		x: 0,
		y: 5,
		deadToLive: []config.Condition{{
			Operator:       "=",
			LiveNeighbsNum: 1,
		}},
		liveToDead: []config.Condition{{
			Operator:       "=",
			LiveNeighbsNum: 1,
		}},
	},
}

func TestRulesXyOutsideOfRange(t *testing.T) {
	t.Parallel()
	logger := zap.NewNop()
	board := logic.NewGameBoardSetup(testBoard)

	for _, set := range ousideOfRangeTestset {
		cfg := config.Config{
			DeadToLive: set.deadToLive,
			LiveToDead: set.liveToDead,
		}

		r := logic.NewRules(logger, cfg)
		assert.False(t, r.IsRuleMet(*board, set.x, set.y), fmt.Sprintf("x: %d, y: %d", set.x, set.y))
	}
}

func TestRulesNotMet(t *testing.T) {
	t.Parallel()
	logger := zap.NewNop()
	board := logic.NewGameBoardSetup(testBoard)

	for _, set := range rulesNotMetTestset {
		cfg := config.Config{
			DeadToLive: set.deadToLive,
			LiveToDead: set.liveToDead,
		}

		r := logic.NewRules(logger, cfg)
		assert.False(t, r.IsRuleMet(*board, set.x, set.y), fmt.Sprintf("x: %d, y: %d", set.x, set.y))
	}
}

func TestRulesMet(t *testing.T) {
	t.Parallel()
	logger := zap.NewNop()
	board := logic.NewGameBoardSetup(testBoard)

	for _, set := range rulesMetTestset {
		cfg := config.Config{
			DeadToLive: set.deadToLive,
			LiveToDead: set.liveToDead,
		}

		r := logic.NewRules(logger, cfg)
		assert.True(t, r.IsRuleMet(*board, set.x, set.y), fmt.Sprintf("x: %d, y: %d", set.x, set.y))
	}
}
