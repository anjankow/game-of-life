package logic

import (
	"errors"
	"fmt"
	"game-of-life/internal/config"
	"sync"

	"go.uber.org/multierr"
	"go.uber.org/zap"
)

type Game struct {
	board   *GameBoard
	cfg     config.Config
	rules   Rules
	logger  *zap.Logger
	started bool
}

func NewGame(logger *zap.Logger, cfg config.Config) Game {
	logger.Info(fmt.Sprintf("init new game, height: %d, width: %d", cfg.Heigth, cfg.Width))
	return Game{
		board:   NewGameBoard(cfg.Heigth, cfg.Width),
		cfg:     cfg,
		rules:   NewRules(logger, cfg),
		logger:  logger,
		started: false,
	}
}

func (l Game) GetGameBoard() GameBoard {
	return *l.board
}

func (l *Game) ToggleCellByUser(x, y int) error {
	if l.started {
		return errors.New("game already started, can't toggle the cells now")
	}
	if x < 0 || x >= l.board.GetHeight() || y < 0 || y >= l.board.GetWidth() {
		return errors.New("invalid coordinates")
	}

	l.board.ToggleCellInitially(x, y)
	return nil
}

func (l Game) canStart() error {
	return nil
	// for _, row := range l.board.currentBoard {
	// 	for _, cell := range row {
	// 		if cell {
	// 			return nil
	// 		}
	// 	}
	// }
	// return errors.New("at least one field has to be set to start")
}

func (l *Game) Start() error {
	if err := l.canStart(); err != nil {
		return err
	}
	l.started = true
	return nil
}

func (l *Game) ChangeSize(x, y int) (err error) {
	if x < config.MinHeight || x > config.MaxHeight {
		err = errors.New("x coordinate is outside of range")
	}
	if y < config.MinWidth || y > config.MaxWidth {
		err = multierr.Append(err, errors.New("y coordinate is outside of range"))
	}
	if err != nil {
		return err
	}

	l.cfg.Width = x
	l.cfg.Heigth = y

	l.New()

	return nil
}

func (l *Game) New() {
	l.started = false
	l.board = NewGameBoard(l.cfg.Heigth, l.cfg.Width)
}

func (l Game) IsStarted() bool {
	return l.started
}

func (l *Game) apppyRules(xStart, xEnd int, wg *sync.WaitGroup) {
	defer wg.Done()
	yEnd := l.board.GetWidth()

	for i := xStart; i < xEnd; i++ {
		for j := 0; j < yEnd; j++ {

			if l.rules.IsRuleMet(*l.board, i, j) {
				l.board.ToggleCellOnNextMove(i, j)
			}
		}
	}
	fmt.Println("applied!")
}

func (l *Game) NextMove() error {
	if !l.IsStarted() {
		return errors.New("the game has to be started first")
	}

	threads := l.cfg.NumOfThreads
	fmt.Println(threads)
	dividedBy := threads - 1
	if dividedBy < 1 {
		dividedBy = 1
	}
	xRange := l.board.GetHeight() / dividedBy

	var wg sync.WaitGroup

	for t := 0; t < threads; t++ {
		xStart := t * xRange
		xEnd := xStart + xRange
		if t == threads-1 {
			xEnd = l.board.GetHeight()
		}
		fmt.Println("adding!")
		wg.Add(1)
		fmt.Println("added!")

		go l.apppyRules(xStart, xEnd, &wg)

	}

	fmt.Println("waiting!")
	wg.Wait()
	fmt.Println("done!")

	l.board.Update()
	fmt.Println("FINITO")

	return nil
}
