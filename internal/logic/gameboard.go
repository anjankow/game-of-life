package logic

import "fmt"

type GameBoard struct {
	height       int
	width        int
	currentBoard [][]bool
	nextBoard    [][]bool
}

func NewGameBoard(height, width int) *GameBoard {
	g := GameBoard{
		height: height,
		width:  width,
	}
	g.currentBoard = newBoard(height, width)
	g.nextBoard = newBoard(height, width)
	return &g
}

func NewGameBoardSetup(board [][]bool) *GameBoard {
	g := GameBoard{
		height:       len(board),
		width:        len(board[0]),
		currentBoard: board,
	}
	return &g
}

func newBoard(height, width int) [][]bool {
	b := make([][]bool, height)
	for i := 0; i < height; i++ {
		b[i] = make([]bool, width)
	}
	return b
}

func (g GameBoard) IsLive(x, y int) bool {
	return g.currentBoard[x][y]
}

func (g GameBoard) GetHeight() int {
	return g.height
}

func (g GameBoard) GetWidth() int {
	return g.width
}

func (g GameBoard) CountLiveNeighbs(x, y int) int {
	currentLiveNeighbs := 0
	for i := x - 1; i <= x+1; i++ {
		for j := y - 1; j <= y+1; j++ {
			if i < 0 || i >= g.height || j < 0 || j >= g.width {
				// outside of range
				continue
			}
			if i == x && j == y {
				// only the neighbourhood matters
				continue
			}

			if g.currentBoard[i][j] {
				currentLiveNeighbs++
			}
		}
	}

	return currentLiveNeighbs
}

func (g GameBoard) CountLivingCells() int {
	num := 0
	for i := 0; i < g.height; i++ {
		for j := 0; j < g.width; j++ {
			if g.currentBoard[i][j] {
				num++
			}
		}
	}

	return num
}

func (g *GameBoard) ToggleCellOnNextMove(x, y int) {
	if g.currentBoard[x][y] {
		g.nextBoard[x][y] = false
	} else {
		g.nextBoard[x][y] = true
	}
}

func (g *GameBoard) ToggleCellInitially(x, y int) {
	if g.currentBoard[x][y] {
		g.currentBoard[x][y] = false
		g.nextBoard[x][y] = false
	} else {
		g.currentBoard[x][y] = true
		g.nextBoard[x][y] = true
	}
}

func (g *GameBoard) Update() {
	fmt.Println("current board <= next board")

	g.currentBoard = *deepCopy(g.nextBoard, g.height, g.width)
}

func deepCopy(arr [][]bool, height, width int) *[][]bool {
	newArr := newBoard(height, width)

	for i := 0; i < height; i++ {
		for j := 0; j < width; j++ {
			newArr[i][j] = arr[i][j]
		}
	}

	return &newArr
}
