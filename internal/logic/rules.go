package logic

import (
	"fmt"
	"game-of-life/internal/config"

	"go.uber.org/zap"
)

type Rules struct {
	cfg    config.Config
	logger *zap.Logger
}

func NewRules(logger *zap.Logger, cfg config.Config) Rules {
	return Rules{
		cfg:    cfg,
		logger: logger,
	}
}

func (r Rules) compare(currentLiveNeighbs int, operator string, cfgLiveNeighbsNum int) bool {
	switch operator {
	case ">":
		return currentLiveNeighbs > cfgLiveNeighbsNum
	case ">=":
		return currentLiveNeighbs >= cfgLiveNeighbsNum
	case "=":
		return currentLiveNeighbs == cfgLiveNeighbsNum
	case "<=":
		return currentLiveNeighbs <= cfgLiveNeighbsNum
	case "<":
		return currentLiveNeighbs < cfgLiveNeighbsNum
	default:
		r.logger.Warn(fmt.Sprintf("hitting default case for rule: %s %d, current number: %d", operator, cfgLiveNeighbsNum, currentLiveNeighbs))
		return false
	}
}

func (r Rules) checkWithRules(liveNeighbs int, rulesList []config.Condition) bool {
	for _, rule := range rulesList {
		if r.compare(liveNeighbs, rule.Operator, rule.LiveNeighbsNum) {
			return true
		}
	}

	return false
}

func (r Rules) IsRuleMet(board GameBoard, x, y int) bool {
	if isOutsideOfRange(board.height, board.width, x, y) {
		return false
	}

	liveNeighbs := board.CountLiveNeighbs(x, y)

	if board.IsLive(x, y) {
		// the cell is live
		return r.checkWithRules(liveNeighbs, r.cfg.LiveToDead)
	} else {
		// the cell is dead
		return r.checkWithRules(liveNeighbs, r.cfg.DeadToLive)
	}
}

func isOutsideOfRange(dimX, dimY, x, y int) bool {
	return x < 0 || x >= dimX || y < 0 || y >= dimY
}
