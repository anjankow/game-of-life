package main

import (
	"fmt"
	"game-of-life/internal/config"
	"game-of-life/internal/controller"
	"game-of-life/internal/logic"
	"game-of-life/internal/server"
	"log"

	"go.uber.org/zap"
)

func main() {
	log.Print("Hello, let the party begin")

	logger, err := zap.NewDevelopment()
	if err != nil {
		log.Fatal(fmt.Errorf("error when setting up the logger: %w", err))
	}
	logger.Info("logger created")

	cfg, err := config.ParseConfig(config.JsonCfgPath, config.EnvFile)
	if err != nil {
		logger.Fatal("error when parsing the configuration file", zap.Error(err))
	}
	logger.Info("configuration read")

	g := logic.NewGame(logger, cfg)
	gameController, err := controller.NewController(logger, cfg, &g)
	if err != nil {
		logger.Fatal("error when creating a new controller", zap.Error(err))
	}

	ser := server.NewServer(logger, &gameController)
	if err := ser.Run(); err != nil {
		logger.Fatal("server error", zap.Error(err))
	}

}
