import './App.css';
import { useState, useEffect } from 'react'
import Board from './components/Board';

import { w3cwebsocket as W3CWebSocket } from 'websocket'
import Resizer from './components/Resizer';

const client = new W3CWebSocket('ws://localhost:7777/ws')


function onCellClicked(i, j) {
  const msg = "toggle " + i + " " + j
  console.log(msg)
  client.send(msg)
}

function onNextMove() {
  const msg = "next"
  console.log(msg)
  client.send(msg)
}

function onStart() {
  const msg = "start"
  console.log(msg)
  client.send(msg)
}

function onResize(height, width) {
  const msg = "resize " + width + " " + height

  console.log(msg)
  client.send(msg)
}

function App() {

  const [board, setBoard] = useState([])
  const [started, setStarted] = useState(false)

  client.onopen = () => {
    console.log('client open!')
    client.send('board?')
  }

  useEffect(() => {
    client.onmessage = (e) => {
      console.log('message! ' + e.data)
      const msg = String(e.data)

      if (msg.startsWith("update")) {
        var words = msg.split(' ')

        const x = words[1]
        const y = words[2]

        var updatedBoard = []
        for (let i = 0; i < x; i++) {
          var row = Array.from({ length: y }, () => { return false })
          updatedBoard.push(row)
        }

        var boardEncoded = words[3]
        var rows = boardEncoded.split('|').slice(1)
        console.log(rows)
        for (const [i, row] of rows.entries()) {

          var cells = row.split(',').slice(1)
          console.log(i)
          console.log(cells)
          for (const [j, cell] of cells.entries()) {
            if (cell === '1') {
              updatedBoard[i][j] = true
            }
          }
        };

        console.log(updatedBoard)
        setBoard(updatedBoard)
      }
    }
  }, []);

  return (
    <>
      <Resizer onButtonClicked={onResize} />
      <div className="NextMove-div">
        <button hidden={!started} onClick={onNextMove} className="NextMove-button">Next move</button>
        <button hidden={started} onClick={() => { setStarted(true); onStart(); }} className="NextMove-button">Start</button>
      </div>
      <Board board={board} onCellClicked={onCellClicked} /></>
  );
}

export default App;
