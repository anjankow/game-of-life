import React from 'react'
import { useState } from 'react'

const liveCellStyle = { backgroundColor: 'green' }
const deadCellStyle = { backgroundColor: '#F5F5DC' }

function getStyle(cell) {
    if (cell === true) {
        return liveCellStyle
    } else {
        return deadCellStyle
    }
}


function Cell({ i, j, cell, onCellClicked }) {
    const [cellState] = useState(cell)

    return (
        <p style={getStyle(cellState)} onDoubleClick={() => { onCellClicked(i, j) }} > Cell: {cell ? "true" : "false"}, ({i}, {j})</p >
    )
}

export default Cell
