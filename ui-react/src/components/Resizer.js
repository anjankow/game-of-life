import React from 'react'
import { useState } from 'react'
import '../Resizer.css'

function Resizer({ onButtonClicked }) {
    const [height, setHeight] = useState('10')
    const [width, setWidth] = useState('10')

    return (
        <div className="Resizer">
            <div className="flex-row">
                <p>Height: </p><div className="gap"></div><input id="height" value={height} onChange={(e) => setHeight(e.target.value)} className="number-input" type="number"></input>
            </div>
            <div className="flex-row">
                <p>Width: </p><div className="gap"></div><input id="width" value={width} onChange={(e) => setWidth(e.target.value)} className="number-input" type="number"></input>
            </div>
            <div className="gap"></div><button className="button" type="submit" onClick={() => { onButtonClicked(height, width) }} >Change the size</button>
        </div >
    )
}

export default Resizer
