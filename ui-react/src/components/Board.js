import React from 'react'
import Cell from './Cell'

const Board = ({ board, onCellClicked }) => {
    return (
        <>
            {board.map((row, i) => (
                row.map((cell, j) => (
                    <Cell key={Math.random()} i={i} j={j} cell={cell} onCellClicked={onCellClicked} />
                ))
            ))}
        </>
    )
}

export default Board
