module game-of-life

go 1.16

require (
	github.com/gorilla/websocket v1.4.2
	github.com/spf13/viper v1.8.1
	github.com/stretchr/testify v1.7.0
	go.uber.org/multierr v1.6.0
	go.uber.org/zap v1.18.1
)
